package cellular;

public class BriansBrain extends GameOfLife{

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }

    @Override
    public CellState getNextCell(int row, int col) {
        switch (getCellState(row, col)) {
            case ALIVE:
                return CellState.DYING;
            case DYING:
                return CellState.DEAD;
            default:
                return countNeighbors(row, col, CellState.ALIVE) == 2 ? CellState.ALIVE : CellState.DEAD;
        }
    }
}
