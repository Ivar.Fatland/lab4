package cellular;

import java.util.Random;
import java.lang.Math;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid next_generation = currentGeneration.copy();
		for (int column = 0; column < currentGeneration.numColumns(); column++) {
			for (int row = 0; row < currentGeneration.numRows(); row++) {
				next_generation.set(row, column, getNextCell(row, column));
			}
		}
		currentGeneration = next_generation;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int alive_neighbor_count = countNeighbors(row, col, CellState.ALIVE);

		if (
			alive_neighbor_count == 3 ||
			alive_neighbor_count == 2 && aliveAt(row, col)
		) {
			return CellState.ALIVE;
		}

		return CellState.DEAD;
	}

	private boolean aliveAt(int row, int col) {
		return getCellState(row, col) == CellState.ALIVE;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	protected int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;

		// Use Math.max/min to ensure r & c is within CellGrid range
		for (int c = Math.max(col-1, 0); c <= Math.min(col+1, numberOfColumns()-1); c++) {
			for (int r = Math.max(row-1, 0); r <= Math.min(row+1, numberOfRows()-1); r++) {
				
				if (c == col && r == row) {continue;}
				
				if (getCellState(r, c) == state) {neighbors ++;}
			}
		}
		return neighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
