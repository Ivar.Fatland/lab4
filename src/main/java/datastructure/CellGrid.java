package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		grid = new CellState[rows][columns];
        for (CellState[] cellStates : grid) {
            Arrays.fill(cellStates, initialState);
        }
	}

    private CellGrid(CellState[][] grid) {
        this.grid = grid;
    }

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellState[][] new_grid_array =  Arrays.stream(grid)
                                        .map(CellState[]::clone)
                                        .toArray(CellState[][]::new);
        return new CellGrid(new_grid_array);
    }
    
}
